{
  const {
    html,
  } = Polymer;
    /**
      `<cells-bfm-filter>` Description.

      Example:

      ```html
      <cells-bfm-filter></cells-bfm-filter>
      ```

      ## Styling
      The following custom properties and mixins are available for styling:

      ### Custom Properties
      | Custom Property     | Selector | CSS Property | Value       |
      | ------------------- | -------- | ------------ | ----------- |
      | --cells-fontDefault | :host    | font-family  |  sans-serif |
      ### @apply
      | Mixins    | Selector | Value |
      | --------- | -------- | ----- |
      | --cells-bfm-filter | :host    | {} |

      * @customElement
      * @polymer
      * @extends {Polymer.Element}
      * @demo demo/index.html
    */
  class CellsBfmFilter extends Polymer.Element {

    static get is() {
      return 'cells-bfm-filter';
    }

    static get properties() {
      return {
        data: {
          type: Object,
          value: () => ({})
        },
        dataFilter: {
          type: Object,
          value: () => ({})
        },
        buttonClearEvent: {
          type: String,
          value: 'clear-tags'
        },
        buttonSendFilterEvent: {
          type: String,
          value: 'send-data-filter'
        }
      };
    }

    _isEqualTo(inputType, type) {
      return inputType === type;
    }

    _clearFilter() {
      this.dispatchEvent(new CustomEvent(this.buttonClearEvent, {
        bubbles: true,
        composed: true
      }));
      const select = this.root.querySelectorAll('cells-select');
      select.forEach(item => {
        item.selected = false;
        item._label = '';
      });
      const input = this.root.querySelectorAll('cells-molecule-input');
      input.forEach(item => {
        item.value = '';
      });
    }
    _setDataFilter(evt) {
      if (evt.target.name) {
        var name = evt.target.name;
        this.dataFilter[evt.target.name] = evt.detail.value;
        this._validButton(name);
      }
    }
    _validButton(name) {
      this.name = name;
      var enable = true;
      if (this.dataFilter && this.dataFilter[name]) {
        enable = false;
      }
      this.data.buttonSendFilter.buttonDisabled = enable;
      this.notifyPath('data.buttonSendFilter.buttonDisabled');
    }
    _sendInfo() {
      this.dispatchEvent(new CustomEvent(this.buttonSendFilterEvent, {
        bubbles: true,
        composed: true,
        detail: this.dataFilter
      }));
    }
  }
  customElements.define(CellsBfmFilter.is, CellsBfmFilter);
}